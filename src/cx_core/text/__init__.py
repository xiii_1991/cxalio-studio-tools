from .code_detecting import detect_encoding
from .tag_replacer import TagReplacer
from .text_utils import *
